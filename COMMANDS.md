# Commands reference

Relevant commands and settings for setting up and using your ElDewrito server.

[[_TOC_]]

## Chat commands

Chat commands can be used by anyone. Some of them need to be enabled by the server host.

| Command           | Controlled by                                                          | Description                                                 |
| ----------------- | ---------------------------------------------------------------------- | ----------------------------------------------------------- |
| **!help**         |                                                                        | Display the list of commands.                               |
| **!endGame**      | `Server.ChatCommandEndGameEnabled`                                     | Starts a vote to end the current game. Type !yes to vote.   |
| **!endRound**     | `Server.ChatCommandEndRoundEnabled`                                    | Starts a vote to end the current round. Type !yes to vote.  |
| **!kick**         | `Server.ChatCommandKickPlayerEnabled`                                  | Starts a vote to kick a player. Type !yes to vote.          |
| **!kickIndex**    | `Server.ChatCommandKickPlayerEnabled`                                  | Starts a vote to kick a player by index. Type !yes to vote. |
| **!kill**         | `Server.KillCommandEnabled`<br>`Server.KillCommandDuringPodiumEnabled` | Kills the player that runs this command.                    |
| **!list**         |                                                                        | Display the list of players in the server.                  |
| **!listPlayers**  |                                                                        | Display the list of players in the server.                  |
| **!mod**          |                                                                        | Display the mod information.                                |
| **!shuffleTeams** | `Server.ChatCommandShuffleTeamsEnabled`                                | Starts a vote to shuffle the teams. Type !yes to vote.      |

## Commands

| Command                    | Description                                                                                                                                                        |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `Game.Start`               | Starts or restarts the game                                                                                                                                        |
| `Game.Stop`                | Stops the game, goes back to lobby                                                                                                                                 |
| `Game.Update`              | Exit the game and update to the latest version with the launcher.<br>**RCON users, beware! This doesn't restart the server automatically.**                        |
| `Game.Version`             | Displays the game's version                                                                                                                                        |
|||
| `Voting.ReloadJson`        | Manually Reloads Json                                                                                                                                              |
|||
| `Player.Kill`              | Kills the player that runs the command                                                                                                                             |
| `Server.Ping`              | Ping a server                                                                                                                                                      |
| `Player.PrintUID`          | Prints the players UID                                                                                                                                             |
|||
| `Server.AddBan`            | Adds to the ban list (does NOT kick anyone)                                                                                                                        |
| `Server.ClearTempBans`     | clears the temp ban list                                                                                                                                           |
| `Server.KickBan`           | Adds to the ban list and kicks the player                                                                                                                          |
| `Server.KickBanIndex`      | Kicks and IP bans a player from the game by index (host only)                                                                                                      |
| `Server.KickBanPlayer`     | Kicks and IP bans a player from the game by name (host only)                                                                                                       |
| `Server.KickBanUid`        | Kicks and IP bans players from the game by UID (host only)                                                                                                         |
| `Server.KickIndex`         | Kicks a player from the game by index (host only)                                                                                                                  |
| `Server.KickPlayer`        | Kicks a player from the game by name (host only)                                                                                                                   |
| `Server.KickTempBanPlayer` | Kicks and temporarily IP bans a player from the game by name (host only)                                                                                           |
| `Server.KickTempBanUid`    | Kicks and temporarily IP bans players from the game by UID (host only)                                                                                             |
| `Server.KickUid`           | Kicks players from the game by UID (host only)                                                                                                                     |
| `Server.ListPlayers`       | Lists players in the game                                                                                                                                          |
| `Server.ListTempBans`      | List all ips that have a temporary ban                                                                                                                             |
| `Server.MutePlayer`        | Mutes Player from Chat                                                                                                                                             |
| `Server.RemoveTempBan`     | Removes a player by index from the temp ban list                                                                                                                   |
| `Server.Unban`             | Removes from the ban list                                                                                                                                          |
| `Server.UnmutePlayer`      | unmutes Player from Chat                                                                                                                                           |
|||
| `Server.LobbyType`         | Changes the lobby type for the server.<br>0 = Campaign<br>1 = Matchmaking<br>2 = Multiplayer<br>3 = Forge<br>4 = Theater                                           |
| `Server.Mod`               | Sets the current tag mod                                                                                                                                           |
| `Server.Mode`              | Changes the network mode for the server.<br>0 = Xbox Live (Open Party)<br>1 = Xbox Live (Friends Only)<br>2 = Xbox Live (Invite Only)<br>3 = Online<br>4 = Offline |
| `Server.PM`                | Sends a pm to a player as the server. First argument is the player name, second is the message in quotes                                                           |
| `Server.PlayersInfo`       | Emblem and Rank info for each player                                                                                                                               |
| `Server.PlayersPingInfo`   | ping info for each player                                                                                                                                          |
| `Server.RefreshMods`       | Searches for mods in the mods directory                                                                                                                            |
| `Server.Say`               | Sends a chat message as the server                                                                                                                                 |
| `Server.ShuffleTeams`      | Evenly distributes players between the red and blue teams                                                                                                          |
| `Server.WebsocketInfo`     | Display the websocket password for the current server                                                                                                              |
|||
| `Server.Announce`          | Announces this server to the master servers                                                                                                                        |
| `Server.Unannounce`        | Notifies the master servers to remove this server                                                                                                                  |
|||
| `Server.Connect`           | Begins establishing a connection to a server                                                                                                                       |
| `Server.Dedicated`         | Used only to let clients know if the server is dedicated or not                                                                                                    |
| `Server.SubmitVote`        | Sumbits a vote                                                                                                                                                     |
| `Voting.CancelVote`        | Cancels the vote                                                                                                                                                   |
| `Game.StartCountdown`      | Starts the game start countdown                                                                                                                                    |


### Advanced

| Command          | Description                       |
| ---------------- | --------------------------------- |
| `Game.TickRate`  | Set the game tick rate in seconds |
| `Time.GameSpeed` | The game's speed                  |

## Settings

### Ports

| Command                   | Default value | Description                                                              |
| ------------------------- | ------------- | ------------------------------------------------------------------------ |
| `Server.GamePort`         | 11774         | The port number used by Halo Online                                      |
| `Server.Port`             | 11775         | The port number the HTTP server runs on, the game uses Server.GamePort   |
| `Game.RconPort`           | 11776         | The port number used for RCON                                            |
| `Server.SignalServerPort` | 11777         | The port the signaling server will listen on. Used for voice chat (VoIP) |

### Details and features

| Command                            | Default value     | Description                                                                                                                                  |
| ---------------------------------- | ----------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| `Player.Name`                      | Random name       | The players ingame name. Shows as "Host" in the server browser.                                                                              |
| `Server.Password`                  |                   | The server password                                                                                                                          |
| `Server.RconPassword`              |                   | Password for the remote console                                                                                                              |
| `Server.Name`                      | HaloOnline Server | The name of the server (limited to 128 characters)                                                                                           |
| `Server.Message`                   |                   | Text to display on the loading screen (limited to 512 chars)                                                                                 |
| `Server.MaxPlayers`                | 16                | Maximum number of connected players                                                                                                          |
| `Server.ShouldAnnounce`            | 1                 | Controls whether the server will be announced to the master servers                                                                          |
| `Server.Countdown`                 | 5                 | The number of seconds to wait at the start of the game                                                                                       |
| `Server.Sprint`                    | 2                 | Controls whether sprint is enabled on the server.<br>0 = Disabled globally<br>1 = Enabled globally<br>2 = Inherited by gametype settings     |
| `Server.UnlimitedSprint`           | 0                 | Controls whether unlimited sprint is enabled on the server                                                                                   |
| `Server.AssassinationEnabled`      | 0                 | Controls whether assassinations are enabled on the server                                                                                    |
| `Server.BottomlessClipEnabled`     | 0                 | Controls whether bottomless clip is enabled on the server                                                                                    |
| `Server.DualWieldEnabled`          | 1                 | Controls whether dual wielding is enabled on the server                                                                                      |
| `Server.EmotesEnabled`             | 0                 | Controls whether players can use emotes                                                                                                      |
| `Server.EmotesDuringPodiumEnabled` | 0                 | Controls whether players can use emotes during podium                                                                                        |
| `Server.HitMarkersEnabled`         | 0                 | Controls whether or not hitmarkers are enabled on this server                                                                                |
| `Server.PodiumEnabled`             | 0                 | should run podium hsc script at game finish.<br>0 = Disabled<br>1 = Enabled<br>2 = Forged maps Only (When podium was placed on a forged map) |
| `Server.PodiumEnabledDuringForge`  | 0                 | should run podium hsc script in forge at game finish                                                                                         |
| `Server.ShowDisconnectedPlayers`   | 0                 | Show disconnected players in the scoreboard                                                                                                  |
| `Server.NearVictoryMusicEnabled`   | 0                 | controls if music should start to play in a match based on the scores of the match                                                           |
| `Server.PostMatchMusicEnabled`     | 0                 | Controls whether the postmatch music will play during podium/postmatch time                                                                  |
| `Server.TempBanDuration`           | 2                 | Duration of a temporary ban (in games)                                                                                                       |

### Chat command settings

| Command                                 | Default value                                         | Description                                                          |
| --------------------------------------- | ----------------------------------------------------- | -------------------------------------------------------------------- |
| `Server.ChatCommandEndGameEnabled`      | 1                                                     | Controls whether or not players can vote to end the game.            |
| `Server.ChatCommandEndRoundEnabled`     | 1                                                     | Controls whether or not players can vote to end the round.           |
| `Server.ChatCommandKickPlayerEnabled`   | 1                                                     | Controls whether or not players can vote to kick someone.            |
| `Server.ChatCommandShuffleTeamsEnabled` | 1                                                     | Controls whether or not players can vote to shuffle the teams.       |
| `Server.KillCommandEnabled`             | 0                                                     | Controls whether players can use the kill                            |
| `Server.KillCommandDuringPodiumEnabled` | 0                                                     | Controls whether players can use the kill during the end game podium |
| `Server.KillCommandMessage`             | "Spartans never die, they're just missing in action." | Sets the message that is sent when a user uses the kill command      |

### Teams

| Command                     | Default value | Description                                                                              |
| --------------------------- | ------------- | ---------------------------------------------------------------------------------------- |
| `Server.TeamShuffleEnabled` | 1             | Controls whether or not the teams will be automatically shuffled before the game starts. |
| `Server.NumberOfTeams`      | 2             | Set the desired number of teams                                                          |
| `Server.TeamSize`           | 1             | Set the minimum number of players each team must have before a new team is assigned      |

### Voting

| Command                            | Default value | Description                                                                                 |
| ---------------------------------- | ------------- | ------------------------------------------------------------------------------------------- |
| `Voting.DuplicationLevel`          | 1             | Whether duplicate voting options will be allowed.                                           |
| `Voting.VoteGameStartCountdown`    | 5             | Controls how many seconds to wait after a vote passes before calling 'game.start'.          |
| `Voting.SystemType`                | 0             | 0 = Disabled, 1 = Voting, 2 = Vetoing                                                       |
| `Voting.VoteTime`                  | 20            | Controls how long the vote lasts for Map Voting.                                            |
| `Voting.MaxRevoteCount`            | 3             | Controls how many revotes are allowed in the voting system                                  |
| `Voting.VoteOptionCount`           | 3             | Controls how many voting options are displayed                                              |
| `Voting.MaxRematchCount`           | 0             | Controls how many rematches are allowed in the voting system                                |
| `Voting.MaxVetoCount`              | 1             | Controls how many veto votes are allowed                                                    |
| `Voting.VetoWinnerShowTime`        | 5             | The length of time the winning option is show                                               |
| `Voting.VetoPassPercentage`        | 50            | Percentage of players that need to vote for it to pass                                      |
| `Voting.VetoSelectionType`         | 0             | 0 = random, 1 = ordered                                                                     |
| `Voting.InstantVoteSkipEnabled`    | 0             | Controls whether instant skip voting is enabled when majority of players vote for an option |
| `Voting.InstantVoteSkipMinPlayers` | 4             | Minimum number of players that need to vote for an instant skip to be considered            |

### File paths

| Command                        | Default value             | Description                                       |
| ------------------------------ | ------------------------- | ------------------------------------------------- |
| `Game.LogName`                 | "logs\dorito.log"         | Sets the name of the file to log game events to   |
| `Server.ChatLogEnabled`        | 1                         | Controls whether chat logging is enabled          |
| `Server.ChatLogFile`           | "logs\chat.log"           | Sets the name of the file to log chat to          |
| `Server.RconLogEnabled`        | 1                         | Controls whether rcon logging is enabled          |
| `Server.RconLogFile`           | "logs\rcon.log"           | Sets the name of the file to log rcon messages to |
| `Server.ListPlayersLogEnabled` | 0                         | Controls whether player list logging is enabled   |
| `Server.ListPlayersLogFile`    | "logs\listplayers.log"    | Sets the name of the file to log player lists to  |
| `Voting.JsonPath`              | "data/server/voting.json" | Voting Json Path                                  |

### Chat anti-spam

| Command                           | Default value | Description                                                                 |
| --------------------------------- | ------------- | --------------------------------------------------------------------------- |
| `Server.FloodFilterEnabled`       | 1             | Controls whether chat flood filtering is enabled                            |
| `Server.FloodMessageScoreShort`   | 2             | Sets the flood filter score for short messages                              |
| `Server.FloodMessageScoreLong`    | 5             | Sets the flood filter score for long messages                               |
| `Server.FloodTimeoutScore`        | 10            | Sets the flood filter score that triggers a timeout                         |
| `Server.FloodTimeoutSeconds`      | 120           | Sets the timeout period in seconds before a spammer can send messages again |
| `Server.FloodTimeoutResetSeconds` | 1800          | Sets the period in seconds before a spammer's next timeout is reset         |

### Advanced settings

| Command                             | Default value | Description                                                                                                           |
| ----------------------------------- | ------------- | --------------------------------------------------------------------------------------------------------------------- |
| `UPnP.Enabled`                      | 1             | Enables UPnP to automatically port forward when hosting a game.                                                       |
| `Server.Http.CacheTime`             | 5             | Time in seconds the server should cache the http server response                                                      |
||||
| `Server.CountdownLobby`             | 3             | The number of seconds to wait in the lobby before the game starts                                                     |
| `Server.HUDWaypointStyle`           | 0             | 0 = Halo Online, 1 = Halo 3                                                                                           |
||||
| `Server.MuteSoundsDuringPostGame`   | 1             | Controls whether the game will mute sounds during the post game time                                                  |
| `Server.ShowPlayerPing`             | 1             | Controls whether the game will show ping on client scoreboards                                                        |
||||
| `Server.NearVictoryMusicPercentage` | 90            | controls when music should start to play in a match based on the required score_to_win for the gametype. 0 = disabled |
| `Server.RconConsoleOutputEnabled`   | 0             | Controls whether the console ouput of the server is forwarded out through rcon                                        |
| `Server.SendChatToRconClients`      | 0             | Controls whether or not chat should be sent through rcon                                                              |
| `Server.ReturnToLobbyTimeout`       | 10            | Controls the maximum duration of blackscreens postgame                                                                |
| `Server.BanListSyncIntervalSeconds` | 60            | The interval at which banlists are synced and enforced                                                                |
| `Server.VotePassPercentage`         | 50            | Percentage of players required to vote yes for a chat command vote to pass                                            |
| `Server.ChatCommandVoteTime`        | 45            | The number of seconds a chat command vote lasts                                                                       |
| `Server.PostGameTimeSeconds`        | 7             | Time in seconds from game over to return to lobby                                                                     |
| `Server.PostGameFadeStartDelay`     | 4             | Time in seconds from game over to start fading to black                                                               |
