$FileName = Split-Path $PSCommandPath -Leaf
$Instance = $fileName.Replace('New dedicated server', '').Replace('.ps1', '').Trim()

if (!$Instance) {
	Write-Warning "Invalid instance ID. Please rename the script as such: `"New dedicated server InstanceName`""
	Pause
	Exit 1
}

if ($Instance -match ' ') {
	Write-Warning "The instance name must not contain spaces. Please avoid special characters too."
	Pause
	Exit 1
}

if (Test-Path "eldorado_$Instance.exe") {
	Write-Warning "An instance with the name `"$Instance`" already exists."
	Write-Warning "Please rename the script with another instance ID."
	Pause
	Exit 1
}

if (! (Test-Path 'eldorado.exe')) {
	Write-Warning "Couldn't locate eldorado.exe. Am I in the right place?"
	Pause
	Exit 2
}

if (! (Test-Path 'dedicated_server.bat')) {
	Write-Warning "Couldn't locate the dedicated server script."
	Write-Warning 'Please verify your installation files.'
	Pause
	Exit 2
}


if (! (Test-Path 'data\dewrito_prefs.cfg')) {
	Write-Warning "Couldn't locate the preferences files."
	Write-Warning 'Please run the game at least once.'
	Pause
	Exit 2
}

Copy-Item 'eldorado.exe' "eldorado_$Instance.exe"
Copy-Item 'data\dewrito_prefs.cfg' "data\dewrito_prefs_$Instance.cfg"

$DedicatedServerScript = Get-Content 'dedicated_server.bat'
$DedicatedServerScript = $DedicatedServerScript.Replace('eldorado.exe', "eldorado_$Instance.exe")
$DedicatedServerScript = $DedicatedServerScript.Replace('-minimized', "-minimized -instance $Instance")
Set-Content -Path "dedicated_server_$Instance.bat" -Value $DedicatedServerScript

# $PreferencesFile = Get-Content "data\dewrito_prefs_$Instance.cfg"
# $PreferencesFile = $PreferencesFile.Replace('HaloOnline Server', 'My first dedicated server')
# Set-Content -Path "data\dewrito_prefs_$Instance.cfg" -Value $PreferencesFile
