$Repository = 'Insan1ty0ne/eldewrito-server-hosting'
$DownloadUrl = "https://gitlab.com/$Repository/-/archive/main/eldewrito-server-hosting-main.zip"
$DownloadPath = "$env:TMP\ElDewrito playlists.zip"
$PlaylistsPath = "$env:TMP\ElDewrito playlists"

$FileName = Split-Path $PSCommandPath -Leaf
$Playlist = $fileName.Replace('.ps1', '') -replace "[^0-9]", ''

if (($Playlist -lt 0) -or ($Playlist -gt 99)) {
	Write-Warning "Invalid playlist ID. Please rename the script so that it contains a single integer, e.g. `"Get playlist 5`""
	Pause
	Exit 1
}

if (Test-Path $PlaylistsPath) {
	Remove-Item $PlaylistsPath -Recurse
}

$ProgressPreference = 'SilentlyContinue'
Write-Host 'Fetching latest variants and playlists...'
Invoke-WebRequest $DownloadUrl -OutFile $DownloadPath | Out-Null
Expand-Archive $DownloadPath $PlaylistsPath | Out-Null
Remove-Item $DownloadPath

$PlaylistsPath = "$env:TMP\ElDewrito playlists"
$AvailablePlaylists = Get-ChildItem "$PlaylistsPath\eldewrito-server-hosting-main" -Directory
$VariantsPath = "$PlaylistsPath\eldewrito-server-hosting-main\0. Stock maps and gametypes"

if (Test-Path $VariantsPath) {
	Write-Host 'Patching map and gametype variants...'
	Copy-Item "$VariantsPath\data" . -Recurse -Force
} else {
	Write-Warning "Couldn't locate map and gametype variants in the repository."
	Pause
	Exit 1
}

foreach ($Folder in $AvailablePlaylists) {
	$Name = $Folder.Name

	if ($Name.StartsWith("$Playlist. ")) {
		$PlaylistFound = $true
		$PlaylistPath = $Folder.FullName

		Write-Host "Installing playlist: $Name"
		Copy-Item "$PlaylistPath\data" . -Recurse -Force
		Write-Host 'Done!'
	}
}

if (! $PlaylistFound) {
	Write-Warning "Couldn't find the specified playlist `"$Playlist`" in the repository."
	Write-Warning 'Please check that it exists at https://gitlab.com/$Repository and try again.'
	Pause
	Exit 1
}

Remove-Item $PlaylistsPath -Recurse
