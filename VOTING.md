# Voting documentation

Creating a voting playlist for ElDewrito allows players to have a say in which maps and game modes they want to play. This documentation will guide you through the process of setting up a voting playlist for your ElDewrito server.

[[_TOC_]]

## File types and location

Different file types are used by ElDewrito for running a game:

- **Maps**, user variants are stored in `.\data\map_variants`
- **Gametypes**, user variants are stored in `.\data\game_variants`
- **Modpacks**, stored in `.\mods` – they can also contain maps and gametypes

You can find mods on the [ElDewrito Modpacks Discord server](https://discord.com/invite/CPc7Gf7TPf).

The voting playlist is controlled by a [JSON](https://www.digitalocean.com/community/tutorials/an-introduction-to-json) file located at `.\data\server\voting.json`. We recommend using a smart text editor like [Notepad++](https://notepad-plus-plus.org/) or [JSON Editor Online](https://jsoneditoronline.org/) to avoid syntax errors.

## Creating a voting playlist

For your convenience, a minimal playlist example is available [here](https://gitlab.com/Insan1ty0ne/eldewrito-server-hosting/-/blob/main/6.%20Minimal%20example/data/server/voting.json).

You can create one from scratch, by following this structure:

```json
{
  "maps": [],
  "types": []
}
```

### The `maps[]` array

The `maps[]` array contains all the maps available by default for all the gametypes. It must contain **at least 2 entries**:

```json
"maps": [
  {
    "mapName": "zanzibar",
    "displayName": "Last Resort"
  },
  {
    "mapName": "riverworld",
    "displayName": "Valhalla"
  }
]
```

A map is designated by its `mapName`. It can be an internal name (see below), or one of the variants available in your `.\data\map_variants` folder. You can forge new maps or download some from the community.

>**Note**: All the map variant files are called `sandbox.map`. You have to specify the folder name in your JSON file.

The following attributes are available for maps:

- `displayName`: customize the name of the map visible to players during voting
- `imageUrl`: custom thumbnail for the map visible in the list
- `minPlayersAllowed`: the minimum amount of players required for the map to show
- `maxPlayersAllowed`: the maximum amount of players required for the map to show
- `randomChance`: defaults to `0.1`. Set a higher value to increase the frequency of the map

### The `types[]` array

The `types[]` array contains the gametypes that are randomly selected by the server. It must contain **at least 2 entries**:

```json
"types": [
  {
    "typeName": "Slayer"
  },
  {
    "typeName": "Team Slayer"
  }
]
```

A gametype is designated by its `typeName`. It can be an internal name (see below), or one of the variants available in your `.\data\game_variants` folder. You can edit an existing gametype by pressing `X` in the Custom Games lobby (then press `X` again to save it).

>**Note**: All the gametype variant files are called `variant.type`. You have to specify the folder name in your JSON file.

The following attributes are available for gametypes:

- `displayName`: the name of the gametype visible to players during voting
- `modPack`: the name of a mod pack that will be loaded when the game starts
- `randomChance`: defaults to `0.1`. Set a higher value to increase the frequency of the gametype
- `specificMaps[]`: another list of maps used exclusively for this gametype ; the top-level `maps[]` array will then be ignored
- `commands[]`: a list of commands that will be executed before the game starts
- `endOfMatchCommands[]`: a list of commands that will be executed after the game has ended
- `characterOverrides[]`: customize the character appearance for each team (e.g. spartans vs. elites)

### Internal maps and gametypes

<details>
<summary><b>Click here to show maps</b></summary>

The following maps are included in the game, and must be referred to by their internal name:

| Map Name    | Internal Name   |
| ----------- | --------------- |
| Diamondback | `s3d_avalanche` |
| Edge        | `s3d_edge`      |
| Guardian    | `guardian`      |
| High Ground | `deadlock`      |
| Icebox      | `s3d_turf`      |
| Last Resort | `zanzibar`      |
| Narrows     | `chill`         |
| Reactor     | `s3d_reactor`   |
| Sandtrap    | `shrine`        |
| Standoff    | `bunkerworld`   |
| The Pit     | `cyberdyne`     |
| Valhalla    | `riverworld`    |
</details>

<details>
<summary><b>Click here to show gametypes</b></summary>

>**Note**: Internal gametypes are case-sensitive! Loading `crazy king` instead of `Crazy King` will fail.

The following gametypes are included in the game, and must be referred to by their internal name:

| Game Type       | Internal Name       |
|---------------- | ------------------- |
| Slayer          | `Slayer`            |
| Team Slayer     | `Team Slayer`       |
| Rockets         | `Rockets`           |
| Elimination     | `Elimination`       |
| Duel            | `Duel`              |
| Team SWAT       | `team_swat`         |
| Skirmish        | `skirmish`          |
|                 |                     |
| Oddball         | `Oddball`           |
| Team Oddball    | `Team Oddball`      |
| Lowball         | `Lowball`           |
| Ninjaball       | `Ninjaball`         |
| Rocketball      | `Rocketball`        |
|                 |                     |
| Crazy King      | `Crazy King`        |
| Team King       | `Team King`         |
| Mosh Pit        | `Mosh Pit`          |
|                 |                     |
| Multi Flag      | `Multi Flag`        |
| One Flag        | `One Flag`          |
| Tank Flag       | `Tank Flag`         |
| Attrition CTF   | `Attrition CTF`     |
|                 |                     |
| Assault         | `Assault`           |
| Neutral Bomb    | `Neutral Assault`   |
| One Bomb        | `One Bomb`          |
| Attrition Bomb  | `Attrition Assault` |
|                 |                     |
| Territories     | `Territories`       |
| Land Grab       | `Land Grab`         |
| Flag Rally      | `Flag Rally`        |
|                 |                     |
| Juggernaut      | `Juggernaut`        |
| Mad Dash        | `Mad Dash`          |
| Ninjanaut       | `Ninjanaut`         |
|                 |                     |
| Infection       | `Infection`         |
| Save One Bullet | `Save One Bullet`   |
| Alpha Zombie    | `Alpha Zombie`      |
| Hide and Seek   | `Hide and Seek`     |
|                 |                     |
| VIP             | `VIP`               |
| One Sided VIP   | `One Sided VIP`     |
| Escort          | `Escort`            |
| Influential VIP | `Influential VIP`   |
</details>

>**Note**: Variant files take priority over internal files. If you have a `.\data\game_variants\Slayer\variant.slayer` file, you will not be able to load the internal mode from the console or voting JSON.

## Enabling voting on your server

Once you have a `voting.json` file ready, you can enable voting in your server from the **Host Settings**:

- Set the **Network Mode** to `Online`
- Scroll down to the **Voting** section, and set **Voting Mode** to `Vote` or `Veto`
- Close the Host Settings. Your playlist should be running. If not, check `.\logs\dorito.log` for errors.

Voting can also be controlled from the console or preferences file:

- `Voting.SystemType 0` disables voting completely (default)
- `Voting.SystemType 1` enables voting
- `Voting.SystemType 2` enables vetoing

Here is a list of relevant commands for configuring the voting on your server:

| Command                            | Default value             | Description                                                                                                                                            |
| ---------------------------------- | ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `Voting.ReloadJson`                |                           | Manually reloads the JSON file.<br>`Voting.ReloadJson 0` will refresh the playlist without parsing the file again (useful for testing large playlists).|
| `Voting.CancelVote`                |                           | Cancels the current vote.                                                                                                                              |
||||
| `Voting.DuplicationLevel`          | 1                         | Whether duplicate voting options will be allowed.                                                                                                      |
| `Voting.VoteGameStartCountdown`    | 5                         | Controls how many seconds to wait after a vote passes before starting the game.                                                                        |
| `Voting.SystemType`                | 0                         | 0 = Disabled, 1 = Voting, 2 = Vetoing                                                                                                                  |
| `Voting.VoteTime`                  | 20                        | Controls how long the vote lasts in seconds.                                                                                                           |
| `Voting.MaxRevoteCount`            | 3                         | Controls how many revotes are allowed in the voting system                                                                                             |
| `Voting.VoteOptionCount`           | 3                         | Controls how many voting options are displayed                                                                                                         |
| `Voting.MaxRematchCount`           | 0                         | Controls how many rematches are allowed in the voting system                                                                                           |
| `Voting.MaxVetoCount`              | 1                         | Controls how many veto votes are allowed                                                                                                               |
| `Voting.VetoWinnerShowTime`        | 5                         | The length of time the winning option is show                                                                                                          |
| `Voting.VetoPassPercentage`        | 50                        | Percentage of players that need to vote for it to pass                                                                                                 |
| `Voting.VetoSelectionType`         | 0                         | 0 = random, 1 = ordered                                                                                                                                |
| `Voting.InstantVoteSkipEnabled`    | 0                         | Controls whether instant skip voting is enabled when majority of players vote for an option                                                            |
| `Voting.InstantVoteSkipMinPlayers` | 4                         | Minimum number of players that need to vote for an instant skip to be considered                                                                       |
| `Voting.JsonPath`                  | "data/server/voting.json" | The path to the voting JSON file used by the server.                                                                                                   |

## Troubleshooting voting errors

If you're having troubles getting your playlist to show in the server voting, please follow the following steps:

1. Make sure your JSON file's syntax is correct with a [linter](https://jsonlint.com/). You can learn more about the JSON format [here](https://www.digitalocean.com/community/tutorials/an-introduction-to-json).
2. Check your `.\logs\dorito.log` file for errors and warnings.
3. Reach out for help on [Discord](https://discord.gg/eldewrito) in `#hosting-help`, we are always happy to help!

## Advanced voting configuration

### Mod packs and `mods.json`

If you want your server to play modded games, you will need to make sure every player has the mods installed before they can join your server.

This is done by having a `.\data\server\mods.json` file that lists all the mod packs and their direct download URL.

Here is a fictional example with two mods, `Mythic` and `ED++`:

```json
{
  "mods": {
    "Mythic": {
      "package_url": "http://example.com/mythic.pak"
    },
    "ED++": {
      "package_url": "http://example.com/ED%2B%2B.pak"
    }
  }
}
```

>**Note**: The URL must point to a **direct download** of the file. You can't use file sharing services that require clicking on a Download button (e.g. Google Drive).

The presence of a `mods.json` file in your ElDewrito installation implies two things:

- When your game starts, it will now check that you have all the mods listed in your own `mods.json`. If your local copy of the mod is outdated, it will be downloaded again.
- When people join your server, they will be prompted to download the same mods if they don't have them installed already.

You can then use the mod names in your `voting.json` gametype entries:

```json
"types": [
  {
    "typeName": "Slayer",
    "modPack": "ED++"
  },
  {
    "typeName": "Team Slayer",
    "modPack": "Mythic"
  }
]
```

### Templates and inheritance

To avoid repetitions, you can define gametype templates that you can re-use later. For example, take this gametype entry:

```json
{
  "isTemplate": true,
  "typeName": "My template",
  "commands": [
    "Server.AssassinationEnabled 0"
  ],
  "endOfMatchCommands": [
    "Server.AssassinationEnabled 1"
  ],
  "specificMaps": [
    {
      "mapName": "guardian",
      "displayName": "Guardian"
    },
    {
      "displayName": "Narrows",
      "mapName": "chill"
    },
    {
      "mapName": "cyberdyne",
      "displayName": "The Pit"
    }
  ]
}
```

Notice the `isTemplate` property. You can now create other gametype entries that inherit all the properties of `My template`:

```json
{
  "inherits": "My template",
  "typeName": "Slayer"
},
{
  "inherits": "My template",
  "typeName": "team_swat",
  "displayName": "Team SWAT"
},
{
  "inherits": "My template",
  "typeName": "Team Oddball",
  "specificMaps": [
    {
      "mapName": "guardian",
      "displayName": "Guardian"
    }
  ]
}
```

### Veto playlist

As mentioned above, you can either use the **Vote** or the **Veto** systems. Instead of showing different options for players to choose from, the **Veto** mode will automatically select one gametype/map combo. Players then have the possibility to veto the selection, and if a majority does, the server will pick another one.

If you want more control about your playlist rotation, you can set a pre-defined veto playlist in your `voting.json` file like so:

```json
{
  "vetoPlaylist": [
    {
      "type": {
        "typeName": "Slayer"
      },
      "map": {
        "displayName": "Guardian",
        "mapName": "guardian"
      }
    },
    {
      "type": {
        "typeName": "Multi Flag",
        "displayName": "Team CTF"
      },
      "map": {
        "displayName": "Valhalla",
        "mapName": "riverworld"
      }
    }
  ]
}
```

For your convenience, a veto playlist example is available [here](https://gitlab.com/Insan1ty0ne/eldewrito-server-hosting/-/blob/main/6.%20Minimal%20example/data/server/veto.json).

You can control the order of the veto playlist from the **Host settings** (find **Veto system selection type**) or the console:

- `Voting.VetoSelectionType 0` to play the modes in a **random** order
- `Voting.VetoSelectionType 1` to play the modes in the same order they are in the JSON

>**Note**: The `vetoPlaylist[]` array is at the **top level** of the JSON file, not inside `maps[]` or `array[]`. It can exist alone too ; if `vetoPlaylist[]` is present, `maps[]` and `types[]` are not required.

### Character overrides

Here is an example of the `characterOverrides[]` gametype attribute:

```json
{
  "typeName": "Team Slayer",
  "characterOverrides": {
    "team0": ["default", "masterchief"],
    "team1": ["default", "dervish"],
    "team2": ["default", "masterchief"],
    "team3": ["default", "dervish"]
  }
}
```

Each key (the value before the colon) represents the team index, from 0 (Red Team) to 7 (Pink Team).

The first item of each array (here, `default`) represents a character set available in the game – which can be modded, if you also use the `modPack` attribute!

The second item is the name of a character available in that set.

This example above is effectively changing the Blue and Orange teams to Covenant Elites, while Red and Green teams remain regular Spartans.
