# ElDewrito Server Hosting

This repository contains several pre-made playlists and useful resources for ElDewrito 0.7 players looking to host their own servers. Each folder contains one playlist and you can only host one playlist on a server at a time.

## Hosting a server

If your router supports UPnP, the port redirections should be created automatically by the game.

If not, you will have to open these ports manually in your router:

- 11774 UDP (game server)
- 11775 TCP (info server, required)
- 11776 TCP (RCON, optional, make sure you set a `Server.RconPassword`)
- 11777 TCP (voice chat, optional)

### Command reference

A list of all the commands that are relevant for hosting is available in [COMMANDS.md](https://gitlab.com/Insan1ty0ne/eldewrito-server-hosting/-/tree/main/COMMANDS.md).

### Dedicated mode

You can run the game in dedicated (headless) mode with `eldorado.exe -dedicated`.

You can either:

- add `-dedicated` in your launcher's launch arguments
- use the `dedicated_server.bat` script in your ElDewrito installation (restart on crash)
- use a script from this repository (useful for multiple instances)
  1. Right-click [**here**](<https://gitlab.com/Insan1ty0ne/eldewrito-server-hosting/-/raw/main/New dedicated server %23.ps1>), and select "Save link as..."
  2. Put this script into the root of your server directory (next to `eldorado.exe`)
  3. Replace `#` in the name by a unique name for the instance.
  4. Right-click on it, and select "Run with PowerShell".
  5. A new `dedicated_server_#.bat` file is created. Run your server with it.
  
  Note: You may have to allow the execution of PowerShell scripts on your system beforehand. [Learn more](https://learn.microsoft.com/powershell/module/microsoft.powershell.security/set-executionpolicy)

In dedicated mode, the game console is not accessible but a system tray icon is added to perform common actions:

<img src="https://i.imgur.com/ScJt1r5.png" alt="ElDewrito's system tray context menu">

### RCON

You can use RCON to send commands to a remote server. Check out [RconTool](https://github.com/jaron780/RconTool)! ([download](https://github.com/jaron780/RconTool/releases/download/4.1/RconTool.exe))

### Docker

A Docker image was made by the community and is available: [domistyle/eldewrito](https://hub.docker.com/r/domistyle/eldewrito) ([source](https://github.com/DomiStyle/docker-eldewrito))

## Installing a playlist

### Automatically (beta)

1. Right-click [**here**](<https://gitlab.com/Insan1ty0ne/eldewrito-server-hosting/-/raw/main/Get playlist %23.ps1>), and select "Save link as..."
2. Place this script into the root of your ElDewrito installation
3. Replace `#` in the name by the ID of the playlist you want. See the folder list above.
4. Right-click on it, and select "Run with PowerShell"

Note: You may have to allow the execution of PowerShell scripts on your system beforehand. [Learn more](https://learn.microsoft.com/powershell/module/microsoft.powershell.security/set-executionpolicy)

### Manually

0. Clone or [**download**](https://gitlab.com/Insan1ty0ne/eldewrito-server-hosting/-/archive/main/eldewrito-server-hosting-main.zip) this repository.
1. Open the [`0. Stock maps and gametypes`](https://gitlab.com/Insan1ty0ne/eldewrito-server-hosting/-/tree/main/0.%20Stock%20maps%20and%20gametypes/data) folder, and **copy** the `data` folder that's in it.
2. **Paste** it into the root of your ElDewrito installation. You can also pick only the relevant maps and gametypes you would like.
3. Open the playlist folder of your choice, and **copy** the `data` folder that's in it.
4. **Paste** it into the root of your ElDewrito installation. This will override your existing voting configuration, if you have one.
5. Boot up your server. Enable voting and you should be all set!

## Making your own playlist

Check out the documentation related to voting in [VOTING.md](https://gitlab.com/Insan1ty0ne/eldewrito-server-hosting/-/tree/main/VOTING.md).

## Support

If you need additional help, you can find us on [Discord](https://eldewrito.org/discord). Check out the `#hosting-help` channel!

## Contributing

This repo is 100% open to contributions. Please submit an issue if a playlist has bugs or crashes, or please submit new playlists if you have made something that the community is enjoying and wants to see more of.

## License

This project is covered under the MIT License for open-source software.
